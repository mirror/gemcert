# Gemcert

THIS PROJECT HAS MOVED!!!

All further development on Gemcert will happen in the repo at https://git.sr.ht/~solderpunk/gemcert

Project description, documentation, news, etc. can be found at gemini://zaibatsu.circumlunar.space/~solderpunk/software/gemcert/

Please DO NOT open issues or pull requests at tildegit.org for this project.  Please update links or bookmarks.